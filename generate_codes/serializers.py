from rest_framework import serializers
from .models import RuleSet, MaxUsesRule, ValidityRule, CouponUser, Discount, Brand, Coupon
from django.db.models import ObjectDoesNotExist
from rest_framework import status


class RuleSetSerializer(serializers.ModelSerializer):
    class Meta:
        model = RuleSet
        fields = '__all__'


class MaxUsesRuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = MaxUsesRule
        fields = '__all__'


class ValidityRuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = ValidityRule
        fields = '__all__'


class CouponUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CouponUser
        fields = '__all__'


class CouponCustomSerializer(serializers.ModelSerializer):
    """
    Custom Serializer for get specific Code that assign to User .
    """
    code = serializers.CharField(read_only=True)

    class Meta:
        model = Coupon
        fields = ["code"]


class DiscountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Discount
        fields = '__all__'


class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = '__all__'


class CouponSerializer(serializers.Serializer):
    discount_id = serializers.IntegerField(read_only=True)
    times_used = serializers.IntegerField(read_only=True)
    code = serializers.CharField(read_only=True)
    rule_set_id = serializers.IntegerField(read_only=True)

    value = serializers.FloatField(required=True, write_only=True)
    is_percentage = serializers.BooleanField(required=True, write_only=True)
    expiration_date = serializers.DateField(required=True, write_only=True)
    is_active = serializers.BooleanField(required=True, write_only=True)
    max_uses = serializers.IntegerField(required=False, default=1, write_only=True)
    uses_per_user = serializers.IntegerField(required=True, write_only=True)
    is_finite = serializers.BooleanField(required=True, write_only=True)
    coupon_numbers = serializers.IntegerField(required=True, write_only=True)
    brand_id = serializers.IntegerField(required=True, write_only=True)

    def validate_brand_id(self, brand_id):
        """
        Validate that metric_type already exists and return the actual object
        :param brand_id: the raw metric_type
        :return: MetricType object
        """
        try:
            return Brand.objects.get(id=brand_id)
        except ObjectDoesNotExist:
            raise serializers.ValidationError(detail=f"Brand ID: {brand_id} doesn't exist")

    def create(self, validated_data):
        """
        Creates a Coupon object using the validated data
        :param validated_data: validated data
        :return: the saved coupon objects
        """
        max_uses_rules = MaxUsesRule(
            max_uses=validated_data.get("max_uses"),
            is_infinite=validated_data.get("is_finite"),
            uses_per_user=validated_data.get("uses_per_user")
        )
        max_uses_rules.save()

        validate_rule = ValidityRule(
            expiration_date=validated_data.get("expiration_date"),
            is_active=validated_data.get("is_active"),
        )
        validate_rule.save()

        discount_value = Discount(
            value=validated_data.get("value"),
            is_percentage=validated_data.get("is_percentage"),
        )
        discount_value.save()

        rule_set = RuleSet(
            max_uses=max_uses_rules,
            validity=validate_rule
        )
        rule_set.save()

        # Create X numbers of Coupons.
        for _ in range(validated_data.get("coupon_numbers")):
            coupon = Coupon(
                brand=validated_data.get("brand_id"),
                discount=discount_value,
                rule_set=rule_set

            )
            coupon.save()

        return status.HTTP_201_CREATED

    class Meta:
        model = Coupon
        fields = '__all__'
