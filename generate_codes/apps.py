from django.apps import AppConfig


class GenerateCodesConfig(AppConfig):
    name = 'generate_codes'
