from django.db import models
from django.utils import timezone

from .helpers import (get_random_code, get_coupon_code_length, get_user_model)


class RuleSet(models.Model):
    """
    RuleSet model for Coupon to have max_uses and validity
    """
    max_uses = models.ForeignKey('MaxUsesRule', on_delete=models.CASCADE, verbose_name="Max uses rule")
    validity = models.ForeignKey('ValidityRule', on_delete=models.CASCADE, verbose_name="Validity rule")

    def __str__(self):
        return f"Rule set {self.id}"

    class Meta:
        verbose_name = "Rule set"
        verbose_name_plural = "Rule sets"


class MaxUsesRule(models.Model):
    """
    MaxUses model for Coupon to know :
    - max_uses - How many times should use it .
    - is_infinite - use coupon forever
    - uses_per_user - How many times should one user use it .
    """
    max_uses = models.BigIntegerField(default=0, verbose_name="Maximum uses")
    is_infinite = models.BooleanField(default=False, verbose_name="Infinite uses?")
    uses_per_user = models.IntegerField(default=1, verbose_name="Uses per user")

    def __str__(self):
        return f"MaxUsesRule {self.max_uses}"

    class Meta:
        verbose_name = "Max Uses Rule"
        verbose_name_plural = "Max Uses Rules"


class ValidityRule(models.Model):
    """
    ValidityRule model for Coupon to know :
    - expiration_date -  Expire date of coupon.
    - is_active - Make Coupon Active.
    """
    expiration_date = models.DateTimeField(verbose_name="Expiration date")
    is_active = models.BooleanField(default=False, verbose_name="Is active?")

    def __str__(self):
        return f"ValidityRule {self.id}"

    class Meta:
        verbose_name = "Validity Rule"
        verbose_name_plural = "Validity Rules"


class CouponUser(models.Model):
    """
    CouponUser model :
    - user -  User ID .
    - coupon - Coupon ID .
    - times_uses - times that need to use coupon .
    """
    user_model = get_user_model()
    user = models.ForeignKey(user_model, on_delete=models.CASCADE, verbose_name="User")
    coupon = models.ForeignKey('Coupon', on_delete=models.CASCADE, verbose_name="Coupon")
    times_uses = models.IntegerField(default=1, editable=False, verbose_name="Times uses")

    def __str__(self):
        return f"{self.user} - {self.coupon}"

    class Meta:
        verbose_name = "Coupon User"
        verbose_name_plural = "Coupon Users"


class Discount(models.Model):
    """
    Discount model:
    - value : Value of discount to coupon.
    - is_percentage : Add % for value or not .
    """
    value = models.IntegerField(default=0, verbose_name="Value")
    is_percentage = models.BooleanField(default=False, verbose_name="Is percentage?")

    def __str__(self):
        if self.is_percentage:
            return f"{self.value}% - Discount"

        return f"${self.value} - Discount"

    class Meta:
        verbose_name = "Discount"
        verbose_name_plural = "Discounts"


class Brand(models.Model):
    """
    Brand model:
    title: Title of the brand.
    description: description of the brand.
    """
    id = models.AutoField(unique=True, primary_key=True)
    title = models.CharField(max_length=50, verbose_name="Brand Title", unique=True)
    description = models.CharField(max_length=250, verbose_name="Brand Description", unique=False)

    def __str__(self):
        return f"{self.title} - {self.description}"

    class Meta:
        verbose_name = "Brand"
        verbose_name_plural = "Brands"


class Coupon(models.Model):
    """
    Coupon model:
    brand: Brand ID
    code: Coupon code
    discount: Discount ID
    times_used : How many times this Coupon Used.
    rule_set: RuleSet ID
    """
    code_length = get_coupon_code_length()
    brand = models.ForeignKey('Brand', on_delete=models.CASCADE)
    code = models.CharField(max_length=code_length, default=get_random_code, verbose_name="Coupon Code", unique=True)
    discount = models.ForeignKey('Discount', on_delete=models.CASCADE)
    times_used = models.IntegerField(default=0, editable=False, verbose_name="Times used")
    created = models.DateTimeField(editable=False, verbose_name="Created")
    rule_set = models.ForeignKey('Ruleset', on_delete=models.CASCADE, verbose_name="Ruleset")

    def __str__(self):
        return self.code

    def use_coupon(self, user):
        coupon_user, created = CouponUser.objects.get_or_create(user=user, coupon=self)
        coupon_user.times_uses = self.rule_set.max_uses.uses_per_user
        coupon_user.save()

        self.times_used += 1
        self.save()

    def get_discount(self):
        return {
            "value": self.discount.value,
            "is_percentage": self.discount.is_percentage
        }

    def get_discounted_value(self, initial_value):
        discount = self.get_discount()

        if discount['is_percentage']:
            new_price = initial_value - ((initial_value * discount['value']) / 100)
            new_price = new_price if new_price >= 0.0 else 0.0
        else:
            new_price = initial_value - discount['value']
            new_price = new_price if new_price >= 0.0 else 0.0

        return new_price

    def save(self, *args, **kwargs):
        if not self.id:
            self.created = timezone.now()
        return super(Coupon, self).save(*args, **kwargs)
