import string
import random

from django.conf import settings


def get_coupon_code_length(length=12):
    """
    Get Coupon code length
    """
    return settings.DSC_COUPON_CODE_LENGTH if hasattr(settings, 'DSC_COUPON_CODE_LENGTH') else length


def get_user_model():
    """
    GET DEFAULT user auth model
    """
    return settings.AUTH_USER_MODEL


def get_random_code(length=12):
    """
    Generate Coupon code
    """
    length = get_coupon_code_length(length=length)
    return f''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(length))
