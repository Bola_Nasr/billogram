from rest_framework import generics
from .serializers import BrandSerializer, CouponSerializer, MaxUsesRuleSerializer, RuleSetSerializer, \
    ValidityRuleSerializer, CouponCustomSerializer, DiscountSerializer, CouponUserSerializer
from .models import RuleSet, MaxUsesRule, ValidityRule, CouponUser, Discount, Brand, Coupon
import datetime


class RuleSetView(generics.ListCreateAPIView):
    serializer_class = RuleSetSerializer
    queryset = RuleSet.objects.all()


class MaxUsesRuleView(generics.ListCreateAPIView):
    serializer_class = MaxUsesRuleSerializer
    queryset = MaxUsesRule.objects.all()


class ValidityRuleView(generics.ListCreateAPIView):
    serializer_class = ValidityRuleSerializer
    queryset = ValidityRule.objects.all()


class CouponUserView(generics.ListCreateAPIView):
    serializer_class = CouponUserSerializer
    queryset = CouponUser.objects.all()


class CouponUserCustomView(generics.ListCreateAPIView):
    serializer_class = CouponCustomSerializer

    def get_queryset(self):
        """
        get specific code depends on brand_id and some rules
        """

        brand_id = self.kwargs.get("brand_id", None)

        query_set = Coupon.objects.all()

        if brand_id:
            query_set = query_set.filter(brand=brand_id)
            for i in query_set:
                # Check:
                # - The Coupon is Active
                # - The Coupon not expired
                # - The Coupon not achieve max_uses .
                if i.times_used < i.rule_set.max_uses.max_uses and i.rule_set.validity.is_active and \
                        i.rule_set.validity.expiration_date.date() > datetime.datetime.now().date():
                    i.use_coupon(self.request.user)
                    return [{"code": i.code}]
            query_set = []

        return query_set


class DiscountView(generics.ListCreateAPIView):
    serializer_class = DiscountSerializer
    queryset = Discount.objects.all()


class BrandView(generics.ListCreateAPIView):
    serializer_class = BrandSerializer
    queryset = Brand.objects.all()


class CouponView(generics.ListCreateAPIView):
    serializer_class = CouponSerializer

    def get_queryset(self):
        """
        fetch query_params from the request and apply filters to the query_set accordingly
        :return: the filtered query_set
        """
        query_set = Coupon.objects.all()

        return query_set
