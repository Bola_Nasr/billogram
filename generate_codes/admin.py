from django.contrib import admin

from .models import RuleSet, MaxUsesRule, ValidityRule, CouponUser, Discount, Brand, Coupon

admin.site.register(RuleSet)
admin.site.register(MaxUsesRule)
admin.site.register(ValidityRule)
admin.site.register(CouponUser)
admin.site.register(Discount)
admin.site.register(Brand)
admin.site.register(Coupon)
