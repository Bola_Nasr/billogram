from django.urls import path, re_path
from .views import RuleSetView, CouponUserView, DiscountView, BrandView, CouponView, MaxUsesRuleView, ValidityRuleView, CouponUserCustomView

urlpatterns = [
    path('brands/', BrandView.as_view(), name="brand"),
    path('coupons/', CouponView.as_view(), name="coupon"),
    path('max_users/', MaxUsesRuleView.as_view(), name='max_users'),
    path('coupons/validate', ValidityRuleView.as_view(), name='validate_coupons'),
    path('discounts/', DiscountView.as_view(), name='discount'),
    path('rules/', RuleSetView.as_view(), name='rules'),
    path('get_discount/<int:brand_id>', CouponUserCustomView.as_view(), name='access_discount'),
    path('users/coupon', CouponUserView.as_view(), name='user_coupons'),
]
