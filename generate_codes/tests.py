from django.test import TestCase
from rest_framework.test import APIClient
from django.urls import reverse
from rest_framework import status
from datetime import date
from random import randrange
from .models import Brand, Coupon


class MetricsViewTestCase(TestCase):
    MAX_USES = [3, 4, 5]
    BRAND_NAMES = ["H&M", "ZARA", "NIKE"]
    TYPES = ["test type 1", "test type 2"]
    EXPIRATION_DATE = [date(2022, 3, 16), date(2023, 9, 12), date(2021, 12, 1), date(2030, 1, 1)]

    def setUp(self):
        self.client = APIClient()
        for i in range(2):
            Brand(title=self.BRAND_NAMES[i], description='mm').save()

        for i in range(3):
            data = {"value": randrange(100),
                    "is_percentage": True,
                    "expiration_date": self.EXPIRATION_DATE[i % 2],
                    "is_active": True,
                    "max_uses": self.MAX_USES[i % 2],
                    "uses_per_user": self.MAX_USES[i % 2],
                    "is_finite": False,
                    "coupon_numbers": 5,
                    "brand_id": (i % 2) + 1}
            self.client.post(reverse('coupon'), data=data)

    def test_get_all_coupons(self):
        """
        test creating a new coupons successfully
        """
        coupon_count = Coupon.objects.count()

        self.assertEquals(coupon_count, 15)

    def test_create_new_coupon(self):
        """
        test creating a new coupon
        """
        old_coupon_count = Coupon.objects.count()
        data = {
            "value": randrange(100),
            "is_percentage": True,
            "expiration_date": self.EXPIRATION_DATE[1],
            "is_active": True,
            "max_uses": self.MAX_USES[1],
            "uses_per_user": self.MAX_USES[1],
            "is_finite": False,
            "coupon_numbers": 1,
            "brand_id": 1  # ID of the brand you are created .
        }
        self.client.post(reverse('coupon'), data=data)
        self.assertEquals(Coupon.objects.count(), old_coupon_count + 1)

    def test_create_coupon_invalid_brand_id(self):
        """
        test creating a coupon with invalid Brand_id, should return error response with a message.
        also shouldn't create a new object
        """
        old_coupon_count = Coupon.objects.count()
        data = {
            "value": randrange(100),
            "is_percentage": True,
            "expiration_date": self.EXPIRATION_DATE[1],
            "is_active": True,
            "max_uses": self.MAX_USES[1],
            "uses_per_user": self.MAX_USES[1],
            "is_finite": False,
            "coupon_numbers": 1,
            "brand_id": 10
        }
        response = self.client.post(reverse('coupon'), data=data)

        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEquals(Coupon.objects.count(), old_coupon_count)

    def test_get_all_rules(self):
        """
        test get all rules
        """

        response = self.client.get(reverse('rules'))

        self.assertEquals(len(response.json()), 3)