# README #

### What is this repository for? ###

This repository was created as an interview challenge for Billogram

### Problem definition ###

Here at Billogram we use Django and Django Rest Framework (DRF) to implement all the APIs used by our Angular client and mobile apps. The input data for our system originates from multiple sources - either directly from user input or via external APIs.

Your mission is to create a Django app using DRF to generate discount codes for brands , assign it to user of the system
And also can fetch all data you have in Database.


### How to start ###
* Use your favorite package manager to install the requirements
    ```
    pip3 install -r requirements.txt
    ```
* Migrate database
    ```
    ./manage.py migrate
    ```
* Create super user
    ```
    ./manage.py createsuperuser
    ```
    follow the prompts to choose an email and password. This will be used to access the admin site
* Run django server locally
    ```bash
    ./manage.py runserver
    ```
    Note: you might need to create super user

* Run the tests
    ```commandline
    ./manage.py test
    ```

### API Options

```json
URL: /billogram/v1/brands/

HTTP 200 OK
Allow: GET, POST, HEAD, OPTIONS
Content-Type: application/json
Vary: Accept

{
    "name": "Brand",
    "description": "",
    "renders": [
        "application/json",
        "text/html"
    ],
    "parses": [
        "application/json",
        "application/x-www-form-urlencoded",
        "multipart/form-data"
    ],
    "actions": {
        "POST": {
            "id": {
                "type": "integer",
                "required": false,
                "read_only": true,
                "label": "Id"
            },
            "title": {
                "type": "string",
                "required": true,
                "read_only": false,
                "label": "Brand Title",
                "max_length": 50
            },
            "description": {
                "type": "string",
                "required": true,
                "read_only": false,
                "label": "Brand Description",
                "max_length": 250
            }
        }
    }
}
```
    
```json
URL: /billogram/v1/coupons/
        
HTTP 200 OK
Allow: GET, POST, HEAD, OPTIONS
Content-Type: application/json
Vary: Accept

{
    "name": "Coupon",
    "description": "",
    "renders": [
        "application/json",
        "text/html"
    ],
    "parses": [
        "application/json",
        "application/x-www-form-urlencoded",
        "multipart/form-data"
    ],
    "actions": {
        "POST": {
            "discount_id": {
                "type": "integer",
                "required": false,
                "read_only": true,
                "label": "Discount id"
            },
            "times_used": {
                "type": "integer",
                "required": false,
                "read_only": true,
                "label": "Times used"
            },
            "code": {
                "type": "string",
                "required": false,
                "read_only": true,
                "label": "Code"
            },
            "rule_set_id": {
                "type": "integer",
                "required": false,
                "read_only": true,
                "label": "Rule set id"
            },
            "value": {
                "type": "float",
                "required": true,
                "read_only": false,
                "label": "Value"
            },
            "is_percentage": {
                "type": "boolean",
                "required": true,
                "read_only": false,
                "label": "Is percentage"
            },
            "expiration_date": {
                "type": "date",
                "required": true,
                "read_only": false,
                "label": "Expiration date"
            },
            "is_active": {
                "type": "boolean",
                "required": true,
                "read_only": false,
                "label": "Is active"
            },
            "max_uses": {
                "type": "integer",
                "required": false,
                "read_only": false,
                "label": "Max uses"
            },
            "uses_per_user": {
                "type": "integer",
                "required": true,
                "read_only": false,
                "label": "Uses per user"
            },
            "is_finite": {
                "type": "boolean",
                "required": true,
                "read_only": false,
                "label": "Is finite"
            },
            "coupon_numbers": {
                "type": "integer",
                "required": true,
                "read_only": false,
                "label": "Coupon numbers"
            },
            "brand_id": {
                "type": "integer",
                "required": true,
                "read_only": false,
                "label": "Brand id"
            }
        }
    }
}
```

```json
URL: /billogram/v1/users/coupon
        
HTTP 200 OK
Allow: GET, POST, HEAD, OPTIONS
Content-Type: application/json
Vary: Accept

{
    "name": "Coupon User",
    "description": "",
    "renders": [
        "application/json",
        "text/html"
    ],
    "parses": [
        "application/json",
        "application/x-www-form-urlencoded",
        "multipart/form-data"
    ],
    "actions": {
        "POST": {
            "id": {
                "type": "integer",
                "required": false,
                "read_only": true,
                "label": "ID"
            },
            "times_used": {
                "type": "integer",
                "required": false,
                "read_only": true,
                "label": "Times used"
            },
            "user": {
                "type": "field",
                "required": true,
                "read_only": false,
                "label": "User"
            },
            "coupon": {
                "type": "field",
                "required": true,
                "read_only": false,
                "label": "Coupon"
            }
        }
    }
}
```

### Usage:
- Need First to **Login through Admin page** .
```
http://localhost:8000/
```
![img_5.png](img_5.png)

- Create Brand
```
http://localhost:8000/billogram/v1/brands/ (POST)
Request body (json):
{
    "title": String,
    "description": String,
}
```
- Generate X discount codes for specific brand.
```
http://localhost:8000/billogram/v1/coupons/ (POST)
Request body (json):
{
    "value": Integer, #Value of discount.
    "is_percentage": Boolean, #Discount value will be percentage or not.
    "expiration_date": Date, #Date of coupon expire.
    "is_active": Boolean, #Coupon will Active now or not.
    "max_uses": Integer, #How many usage of this coupon.
    "uses_per_user": Integer, #How many one user will use this coupon.
    "is_finite": Boolean, #Use this coupon with any limit.
    "coupon_numbers": Integer, #How many coupons you need to generate.
    "brand_id": Integer #ID of the brand you are created .
}

```

- Get a discount code to the user of the system
```
http://localhost:8000/billogram/v1/get_discount/{brand_id} (GET)
Response body (json):
{
    "code": String 
}
```
- Get all coupons
```
http://localhost:8000/billogram/v1/coupons/ (GET)
Response body (json):
[
  {
   "discount_id": Integer,
   "times_used": Integer,
   "code": String,
   "rule_set_id": Integer
  }
]
```
- Another API's Can use it:

![img.png](img.png)

- Using Django Rest UI:

![img_4.png](img_4.png)
## Admin Pages:
```http://localhost:8000/admin/```
- All Tables in DB:

![img_1.png](img_1.png)

- All Coupons that created, You can Create,Edit and delete though this page:

![img_2.png](img_2.png)

- Max uses rule page:

![img_3.png](img_3.png)
